


( () => {



    module.exports = ( grunt ) => {



        require( 'load-grunt-tasks' )( grunt )



        var gruntInitConfig = {



            sass: {
                styles: {
                    files: [
                        {
                            expand: true,
                            cwd: 'src/sass',
                            src: [
                                '**/*.{sass,scss}'
                            ],
                            dest: 'dist/',
                            ext: '.css'
                        }
                    ]
                }
            },



            stylus: {
                styles: {
                    files: [
                        {
                            expand: true,
                            cwd: 'src/stylus',
                            src: [
                                '**/*.styl',
                                '!**/_*.styl'
                            ],
                            dest: 'dist/',
                            ext: '.css'
                        }
                    ]
                }
            },



            cssmin: {
                styles: {
                    files: [
                        {
                            expand: true,
                            cwd: 'dist',
                            src: [
                                '**/*.css',
                                '**/*.min.css',
                            ],
                            dest: 'dist',
                            ext: '.min.css'
                        }
                    ]
                }
            },



            postcss: {
                options: {
                    processors: [
                        require( 'autoprefixer' )( { browsers: 'last 150 versions' } ) // fucking google chrome :P
                    ]
                },
                styles: {
                    files: [
                        {
                            expand: true,
                            cwd: 'dist',
                            src: [
                                '**/*.css',
                                '!**/*.min.css'
                            ],
                            dest: 'dist',
                            ext: '.css'
                        }
                    ]
                }
            }
        }



        grunt.initConfig( gruntInitConfig )



        grunt.registerTask( 'post-compile', [
            'postcss',
            'cssmin',
        ] )



        grunt.registerTask( 'default', [
            'sass',
            'post-compile'
        ] )



        grunt.registerTask( 'compile-stylus', [
            'stylus',
            'post-compile'
        ] )
    }
} )()
